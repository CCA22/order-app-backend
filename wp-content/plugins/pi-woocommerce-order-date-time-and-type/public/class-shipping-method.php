<?php
class pisol_dtt_shipping_method{
    function __construct(){
        $this->type = pi_dtt_delivery_type::getType();
        if($this->type == 'pickup'){
            add_filter( 'woocommerce_cart_needs_shipping_address', '__return_false');
            add_filter( 'woocommerce_product_needs_shipping','__return_false');
        }
    }
}

add_action('wp_loaded', function(){
    $pisol_disable_dtt_completely = apply_filters('pisol_disable_dtt_completely',false);
    if($pisol_disable_dtt_completely){
        return ;
    }

    new pisol_dtt_shipping_method();
});