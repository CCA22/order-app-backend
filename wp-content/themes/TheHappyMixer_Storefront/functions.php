<?php

/**
 * engine room
 *
 * @package thehappymixer
 */

/**
 * Initialize all the things.
 */
//require get_template_directory() . '/inc/init.php';

/**
 * Add another email recipient for admin New Order emails if a shippable product is ordered
 *
 * @param string $recipient a comma-separated string of email recipients (will turn into an array after this filter!)
 * @param \WC_Order $order the order object for which the email is sent
 * @return string $recipient the updated list of email recipients
 */
function conditional_email_recipient( $recipient, $order ) {
	// just in case
	if ( ! $order instanceof WC_Order ) {
		return $recipient; 
	}
	$order_id = $order->id;
	$new_order_email_to = get_post_meta($order_id, 'new_order_email_to', true);
	$recipient .= ',' . $new_order_email_to;
	return $recipient;
}
add_filter( 'woocommerce_email_recipient_new_order', 'conditional_email_recipient', 10, 2 );

add_action('init', 'nt_cors_enable');
function nt_cors_enable()
{
  header("Access-Control-Allow-Origin: " . get_http_origin());
  header("Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE");
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Allow-Headers: Authorization, Content-Type");
  header("Access-Control-Expose-Headers: x-wc-totalpages, x-wc-total", false);
  if ('OPTIONS' == $_SERVER['REQUEST_METHOD']) {
    status_header(200);
    exit();
  }
}