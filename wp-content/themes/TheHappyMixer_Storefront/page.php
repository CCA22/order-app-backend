<?php
/**
 * Description: custom page template
 *
 * @package thehappymixer
 * @subpackage thehappymixer
 */
get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

<div class="<?php if(is_page('menu')){ ?>col1<?php } else { ?>full-width<?php } ?>">
  <?php the_content(); ?>
  
  <?php 
      if(is_page('home')) 
      {
  ?>
    <div class="brown-dotted-border"></div>
    <div class="what-we-bake-container clearfix">
      <div class="what-we-bake-text">
        <span>
          We are a 100% gluten free bakery that makes cookies, brownies, muffins, doughnuts and much more!</span>
        <a href="/menu">View Our Menu</a>
      </div>
      <div class="what-we-bake-images">
        <img src="<?php echo get_template_directory_uri();?>/images/thumbnails/bottomthumbs.png" />
      </div>
    </div>
    <?php 
     }
     ?>
   </div>
  <?php
    if(is_page('menu'))
    {
    ?>
<div class="col2 non-mobile">
  <div>
    <img src="https://www.thehappymixer.com/wp-content/uploads/2017/04/IMG_1582.jpg" />
  </div>
  <div>
    <img src="https://www.thehappymixer.com/wp-content/uploads/2017/04/IMG_35491.jpg" />
  </div>
  <div>
    <img src="https://www.thehappymixer.com/wp-content/uploads/2017/04/IMG_1473.jpg" />
  </div>
  <div>
    <img src="https://www.thehappymixer.com/wp-content/uploads/2017/04/IMG_16531.jpg" />
  </div>
  <div>
    <img src="https://www.thehappymixer.com/wp-content/uploads/2017/04/IMG_35191.jpg" />
  </div>
  <div>
    <img src="https://www.thehappymixer.com/wp-content/uploads/2017/04/IMG_34811.jpg" />
  </div>
  <div>
    <img src="https://www.thehappymixer.com/wp-content/uploads/2017/04/IMG_31381.jpg" />
  </div>
  <div>
    <img src="https://www.thehappymixer.com/wp-content/uploads/2017/04/IMG_36561.jpg" />
  </div>
  <div>
    <img src="https://www.thehappymixer.com/wp-content/uploads/2014/11/Seashell-Wedding-Cake1.jpg" />
  </div>
</div>
  <?php } ?>
  <?php //if(is_front_page()) { the_content(); }?>
  <?php //edit_post_link(__('Edit', 'bootstrapwp'), '<span class="edit-link">', '</span>'); ?>
  <?php endwhile; // end of the loop. ?>
</div>
<!--//end content-->
<?php get_footer(); ?>




