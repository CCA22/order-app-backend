<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package happymixer
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> <?php //storefront_html_tag_schema(); ?>>
<head>
	<title><?php wp_title('|', true, 'right'); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/vex/vex.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/vex/vex-theme-os.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/style.css?v=1" />
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400' rel='stylesheet' type='text/css'>
		

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>    
	<link rel="icon" href="<?php echo get_template_directory_uri();?>" type="image/x-icon" />    
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>" type="image/x-icon" />
	<?php wp_head(); ?>	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/cookies.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/moment.min.js"></script>	
	<style>
		.what-we-bake-container .what-we-bake-text a:hover {
			text-decoration: underline;
		}
		.stay-connected a i {
			background: #EBBC5A !important;
		}
		.stay-connected a i.fa-facebook {
			font-size: 18px;
			background: #205D7A !important;
		}
		.stay-connected a i.fa-envelope-o {
			padding-top: 5%;
			font-size: 18px;
		}
		img.number-one-badge {
	            position: absolute;
		    right: 151px;
		    top: 27px;
		}
		*[disabled] {
			opacity: .4;
			cursor: not-allowed !important;
		}

		img.start-your-franchise {
			position: absolute;
			right: 41px;
			top: 10px;
		}
		/*#menu-item-387, .site-header-cart-links { display: none !important; }*/
		span.select2-selection__rendered { display: none !important; }
		.address-and-phone { padding-bottom: 30px; }		
		.address-and-phone > span { font-size: 0.9em; margin-bottom:3px; }
		.address-and-phone > span:last-child { margin-bottom:0; }
		.address-and-phone > span span {color: #E68F1A; margin:0 4px; display: inline-block; }
		.mg5 {margin-bottom:10px !important; }
		.what-we-bake-container .what-we-bake-text span { font-size: 1em; }
			#header .header-middle {
			    padding: 45px 0 30px 0 !important;
			}
	</style>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '268096577470139'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=268096577470139&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
	<meta name="facebook-domain-verification" content="flp3ib23co4lfz0wbhh923jv6i4bj9" />
	</head>
<body <?php body_class(); ?>>
<?php include_once("google-analytics.php") ?> 
<div id="page" class="page-wrap">
		  <div id="header-top">
			<div class="container">		
				<div class="row">
					<div class="header-social-container">
							<!--
							<ul class="no-padding">
								<li>
									<a href="https://www.facebook.com/TheHappyMixerGlutenFreeBakery/" target="_blank"><i class="fa fa-facebook"></i></a>
								</li>
							</ul>
							-->
						</div>						
						<div class="header-top-links-container">
						<ul class="header-top-links">							
							<li class="header-account">
							<a href="https://www.toasttab.com/thehappymixerchalfont/marketing-signup" target="_blank">Join Our Email List to Hear about NEW PRODUCTS First!</a>
								  	<a href="https://www.thehappymixer.com/allergen-statement-and-information">Allergen Statement and Information</a>
							<a href="<?php echo get_site_url(); ?>/careers">Employment Opportunities</a>
						<?php 
							if( is_user_logged_in() ) {
						?>	
							<a href="<?php echo get_site_url(); ?>/my-account">My Account</a>
							<a href="<?php echo get_site_url(); ?>/my-account/customer-logout">Sign Out</a>							
						<?php	}  else { ?>							
							<a href="<?php echo get_site_url(); ?>/my-account">Sign In</a>								
						<?php } ?></li>
							<li class="header-cart"><div id="site-header-cart" class="shopping-cart-container">		    
							<ul class="site-header-cart-links">
								<li>									
									<a href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" class="link-cart"><i class="fa fa-shopping-cart"></i> Cart </a>
									<span class="count"><?php echo wp_kses_data( sprintf( _n( '%d', '%d', WC()->cart->get_cart_contents_count()), WC()->cart->get_cart_contents_count() ) );?></span>					
								</li>					
							</ul>
							<div class="site-header-cart-wrapper non-mobile">
								<?php 					
									the_widget( 'WC_Widget_Cart', 'title=' ); 
								?>
							</div>	
							</div>	
							</li>
						</ul>		
						</div>				  	
				</div>	
			</div>
		  </div>
  <div class="main-wrap main-wrap-shadow">
      <header id="header">
          <div class="header-middle">
			  <div class="container">
			  <div class="address-and-phone non-mobile">
                <span>Chalfont<span>|</span><a href="tel:267-663-7209">267-663-7209</a><span>|</span><a href="mailto:chalfont@thehappymixer.com">chalfont@thehappymixer.com</a></span>
                <span>Newtown<span>|</span><a href="tel:215-860-1989">215-860-1989</a><span>|</span><a href="mailto:newtown@thehappymixer.com">newtown@thehappymixer.com</a></span>
            </div>
			<div class="stay-connected non-mobile">
							<!-- <span>Stay Connected</span>  -->
							<!-- <a href="https://www.facebook.com/TheHappyMixerGlutenFreeBakery/" target="_blank"><i class="fa fa-facebook"></i></a>
				<a href="mailto:info@thehappymixer.com"><i class="fa fa-envelope-o beige"></i></a> -->
							<img class="number-one-badge non-mobile" src="<?php echo get_template_directory_uri(); ?>/images/numOneBadge100.png" />
							<a href="https://www.thehappymixerfranchising.com" target="_blank"><img class="start-your-franchise non-mobile" src="<?php echo get_template_directory_uri() ?>/images/franchise2.png" /></a>
						</div>
						<div class="header-mobile mobile">
						<div class="logo">
							<a href="/"><img class="mobile-logo" src="<?php echo get_template_directory_uri();?>/images/logo-mobile.png"/></a>
						</div>
						<div class="hidden">
						<div class="menu-wrap">
							<nav class="menu-mobile mobile-menu-vertical menu-right">
									<li>Home</li>
									<li>About</li>
									<li>Menu</li>
									<li>Location</li>
									<li>FAQ</li>
									<!-- <li>Shop</li> -->
									<li>Where to buy</li>								
							</nav>
							<button class="close-button" id="close-button"></button>
						</div>
						<button class="menu-button" id="open-button"></button>
						</div>
					</div>
			  </div>
		  </div>
      </header>
      <div class="nav-primary-wrap">
	  <div class="left-ribbon"></div>
          <nav id="nav" class="nav-primary" role="navigation">
              <div class="navbar-header" data-target="#navbarCollapse" data-toggle="collapse">
                  <button type="button" class="navbar-toggle">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <a href="#" class="navbar-brand">Navigate</a>
              </div>
              <div id="navbarCollapse" class="collapse navbar-collapse no-padding">
				  <?php
					$menu_args = array (
					  'container' => false,
					  'menu'    => '',
					  'show_home'   => true,
							'menu_class' => 'navbar-nav', 'nav'
						);
						wp_nav_menu($menu_args)
				  ?>
              </div>
              <?php //wp_nav_menu(); ?>
          </nav>
		  <div class="right-ribbon"></div>
      </div>
      <div class="content-wrap clearfix">	
	  	<?php if(is_page('home')) { ?>
			<div style="text-align:center;width:100%;">
			<h3><strong>&ldquo;Bringing back the old-fashioned bakery experience to the gluten free community!&rdquo;<sup style="font-size: 11px;">TM</sup></strong></h3>
			    </div>
	  	<?php } ?>

		  <div class="shipping-notice" style="display: block; padding: 20px; margin-bottom: 20px; background-color: #fff;">
			  <span></span>
			  <p style="margin-bottom: 1rem;"><strong>Thank you for your interest in our goodies! <br /> Due to the national emergency COVID-19 virus, we have suspended shipping temporarily.</strong></p>
			  <p style="margin-bottom: 1rem; display:none;"><strong>For that reason, we have temporarily placed a hold on shipping orders and will resume shipping as usual starting January 7th, 2020. We apologize for any inconvenience.</strong></p>
			  <p style="margin-bottom: 1rem; display:none;"><strong>If you have any further questions, please give us a call at 267-663-7209.  Happy Holidays from The Happy Mixer Family!</strong></p>
		  </div>
		  
