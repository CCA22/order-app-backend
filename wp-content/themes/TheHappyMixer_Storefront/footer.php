</div>
<!-- .main-wrap -->
<footer id="footer" role="contentinfo">
  <div class="footer-wrap">

    <div class="container">
      <div class="shop-container col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <h3>shop</h3>
        <ul>
          <li>
            <a href="https://www.thehappymixer.com/product-category/breads">Breads</a>
          </li>
          <li>
            <a href="https://www.thehappymixer.com/product-category/brownies">Brownies</a>
          </li>
          <li>
            <a href="https://www.thehappymixer.com/product-category/poundcakes">Pound Cake</a>
          </li>
          <li>
            <a href="https://www.thehappymixer.com/product-category/cookies">Cookies</a>
          </li>
          <li>
            <a href="https://www.thehappymixer.com/product-category/cupcakes">Cupcakes</a>
          </li>
          <li>
            <a href="https://www.thehappymixer.com/product-category/delights">Delights</a>
          </li>
          <!--
          <li>
            <a href="https://www.thehappymixer.com/product-category/doughnuts">Doughnuts</a>
          </li>
          -->
          <li>
            <a href="https://www.thehappymixer.com/product-category/kiffles">Kiffles</a>
          </li>
          <!--
          <li>
            <a href="https://www.thehappymixer.com/product-category/muffins">Muffins</a>
          </li>
          -->
          <li>
            <a href="https://www.thehappymixer.com/product-category/vegan">Vegan</a>
          </li>
        </ul>
      </div>
      <div class="location-container col-lg-3 col-md-3 col-sm-3 col-xs-12">
      	<div itemscope itemtype="http://schema.org/Restaurant">
	        <h3>our location</h3>
		<p><span itemprop="name">The Happy Mixer Gluten Free Bakery</span></p>
	        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
		<p><span itemprop="streetAddress">4275 County Line Rd</span></p>
		<p><span itemprop="addressLocality">Chalfont</span>, <span itemprop="addressRegion">PA</span> <span itemprop="postalCode">18914</span></p>
        </div>
		<p><span><a itemprop="telephone" href="tel:+12676637209">267-663-7209</a></span></p>
	</div>
      </div>
      <div class="help-container col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <h3>let us help you</h3>
        <ul>
          <li>
            <a href="https://www.thehappymixer.com/my-account">Your Account</a>
          </li>
          <li>
            <a href="https://www.thehappymixer.com/my-account/view-order">Your Orders</a>
          </li>
          <li>
            <a href="https://www.thehappymixer.com/shipping-return-policy">Shipping &amp; Return Policy</a>
          </li>
		   <li>
            <a href="https://www.thehappymixer.com/going-gluten-free">Guide to Going Gluten Free</a>
          </li>
	  <li>
	  	<a href="https://www.thehappymixer.com/allergen-statement-and-information">Allergen Statement and Information</a>
		</li>
        </ul>
      </div>

      
      <div class="social-container col-lg-3 col-md-3 col-sm-3 col-xs-12" style="display: none;">
        <h3>stay connected</h3>
        <ul>
          <li>
            <a href="https://www.facebook.com/TheHappyMixerBakery" target="_blank">
              <i class="fa fa-facebook"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="copyright">
      <span>
        &copy; 2013 - <?php echo date("Y"); ?> The Happy Mixer Gluten Free Bakery. All Rights Reserved.
      </span>
    </div>
  </div>
</footer>
</div>
<!--/.page-wrap-->
<?php  
    /**
     * Fix for missing wp admin bar
     */
     wp_footer(); 
?>
<script src="//cdn.jsdelivr.net/modernizr/2.7.1/modernizr.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/vex/vex.combined.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.matchHeight.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/classie.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/base.js?v=13.1"></script>
</body>
</html>