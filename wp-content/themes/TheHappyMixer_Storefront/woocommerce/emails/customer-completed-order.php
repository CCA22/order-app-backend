<?php

/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

if (!defined('ABSPATH')) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action('woocommerce_email_header', $email_heading, $email);


$order_id = $order->get_id();

$date = get_post_meta($order_id, 'pi_delivery_date_formatted', true);
$time = get_post_meta($order_id, 'pi_delivery_time', true);
$name = get_post_meta($order_id, 'pickup_location_label', true);
$street = get_post_meta($order_id, 'pickup_location_street', true);
$cityStateZip = get_post_meta($order_id, 'pickup_location_cityStateZip', true);
$phone = get_post_meta($order_id, 'pickup_location_phone', true);

?>

<?php /* translators: %s: Customer first name */ ?>
<p style="display: none;"><?php printf(esc_html__('Hi %s,', 'woocommerce'), esc_html($order->get_billing_first_name())); ?></p>
<p style="display: none;"><?php esc_html_e('We have finished processing your order.', 'woocommerce'); ?></p>
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td bgcolor="#ffffff" width="500" align="center" style="font-family:'Open sans',Arial,sans-serif;line-height:28px;color:#434a47;font-size:18px;padding:0 50px">
				<b>Thank you!</b>
				<br>
				Here&rsquo;s your pickup information:
				<br>
				<br>
				<div style="font-family:'Open sans',Arial,sans-serif;font-weight:bold">
					The Happy Mixer <?php echo $name ?> <br />
					<? echo $street ?> <br />
					<? echo $cityStateZip ?> <br />
				</div>
			</td>
		</tr>
	</tbody>
</table>
<table width="600" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding:0 50px">
	<tbody>
		<tr>
			<td height="24">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td height="1" style="font-size:1px;line-height:1px">
				<hr>
				<!-- <img src="https://order.thehappymixer.com/backend/wp-content/uploads/2021/09/pixel.gif" width="25" height="1" alt="" border="0" style="display:block"> -->
			</td>
		</tr>
		<tr>
			<td height="24">
				&nbsp;
			</td>
		</tr>
	</tbody>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
	<tbody>
		<tr>
			<td bgcolor="#ffffff" width="500" align="center" style="font-family:'Open sans',Arial,sans-serif;font-size:19px;line-height:45px;color:#434a47">
				<span style="line-height:36px">
					Your order will be ready for pickup on:
				</span>
				<div style="font-family:'Open sans',Arial,sans-serif;font-weight:bold;font-size:24px;text-decoration:none">
					<? echo $date ?> <br /> between <br /> <? echo $time ?>
				</div>
			</td>
		</tr>
	</tbody>
</table>
<table width="600" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding:0 50px">
	<tbody>
		<tr>
			<td height="24">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td height="1" style="font-size:1px;line-height:1px">
				<hr>
				<!-- <img src="https://order.thehappymixer.com/backend/wp-content/uploads/2021/09/pixel.gif" width="25" height="1" alt="" border="0" style="display:block"> -->
			</td>
		</tr>
		<tr>
			<td height="24">
				&nbsp;
			</td>
		</tr>
	</tbody>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td bgcolor="#ffffff" align="center" width="500">
				<img src="https://order.thehappymixer.com/backend/wp-content/uploads/2021/09/question-bubble.png" alt="" width="43" height="43" style="font-family:Arial,sans-serif;color:#4c4c4c;font-size:20px;display:block" border="0">
			</td>

		</tr>
		<tr>
			<td bgcolor="#ffffff" width="500" align="center" style="font-family:'Open sans',Arial,sans-serif;font-size:16px;line-height:24px;color:#4c4c4c;padding:0 50px">
				<b>Order Questions?</b>
				<br><br>
				Call <span style="color:#4c4c4c"><?php echo $phone ?></span>
				for an update.
			</td>
		</tr>
	</tbody>
</table>
<table width="600" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding:0 50px">
	<tbody>
		<tr>
			<td height="24">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td height="1" style="font-size:1px;line-height:1px">
				<hr>
				<!-- <img src="https://order.thehappymixer.com/backend/wp-content/uploads/2021/09/pixel.gif" width="25" height="1" alt="" border="0" style="display:block"> -->
			</td>
		</tr>
		<tr>
			<td height="24">
				&nbsp;
			</td>
		</tr>
	</tbody>
</table>
<?php
/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action('woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email);

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action('woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email);

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action('woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email);

/**
 * Show user-defined additional content - this is set in each email's settings.
 */
if ($additional_content) {
	echo wp_kses_post(wpautop(wptexturize($additional_content)));
}

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action('woocommerce_email_footer', $email);
