<?php
$order_id = $order->get_id();
$date = get_post_meta($order_id, 'pi_delivery_date_formatted', true);
$time = get_post_meta($order_id, 'pi_delivery_time', true);
$location = get_post_meta($order_id, 'pickup_location', true);
$phone = get_post_meta($order_id, 'pickup_location_phone', true);
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>" />
	<title><?php echo get_bloginfo('name', 'display'); ?></title>
</head>

<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
	<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'; ?>">
		<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
			<tr>
				<td align="center" valign="top">
					<div id="template_header_image">
						<?php
						if ($img = get_option('woocommerce_email_header_image')) {
							echo '<p style="margin-top:0;"><img src="' . esc_url($img) . '" alt="' . get_bloginfo('name', 'display') . '" /></p>';
						}
						?>

						<!-- echo wp_kses_post( $before . sprintf( __( '[Order #%s]', 'woocommerce' ) . $after . ' (<time datetime="%s">%s</time>)', $order->get_order_number(), $order->get_date_created()->format( 'c' ), wc_format_datetime( $order->get_date_created() ) ) ); -->

					</div>
					<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container">
						<tr>
							<td align="center" valign="top">
								<!-- Header -->
								<table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_header">
									<tr>
										<td id="header_wrapper">
											<h1><?php echo $email_heading; ?></h1>
										</td>
									</tr>
								</table>
								<!-- End Header -->
							</td>
						</tr>
						<tr>
							<td align="center" valign="top">
								<!-- Body -->
								<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
									<tr>
										<td valign="top" id="body_content">
											<!-- Content -->
											<table border="0" cellpadding="20" cellspacing="0" width="100%">
												<tr>
													<td valign="top">
														<div id="body_content_inner">
															<table width="600" border="0" cellspacing="0" cellpadding="0">
																<tbody>
																	<tr>
																		<td bgcolor="#ffffff" width="500" align="center" style="font-family:'Open sans',Arial,sans-serif;line-height:28px;color:#434a47;font-size:18px;padding:0 50px">
																			<b>Thank you!</b>
																			<br>
																			Here&rsquo;s your pickup information:
																			<br>
																			<br>
																			<div style="font-family:'Open sans',Arial,sans-serif;font-weight:bold">
																				<b>The Happy Mixer
																					<? echo $location ?>
																				</b>
																			</div>
																		</td>
																	</tr>
																</tbody>
															</table>

															<table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
																<tbody>
																	<tr>
																		<td width="50" bgcolor="#ffffff">
																		</td>
																		<td>
																			<table width="500" border="0" cellspacing="0" cellpadding="0">
																				<tbody>
																					<tr>
																						<td bgcolor="#ffffff" width="500" align="center" style="font-family:'Open sans',Arial,sans-serif;font-size:19px;line-height:45px;color:#434a47">
																							<span style="line-height:36px">
																								Your order will be ready for pickup on:
																							</span>
																							<div style="font-family:'Open sans',Arial,sans-serif;font-weight:bold;font-size:24px;text-decoration:none">
																							<? echo $date ?> <br/> between <br /> <? echo $time ?>
																							</div>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																		<td width="50" bgcolor="#ffffff">

																		</td>
																	</tr>
																</tbody>
															</table>

															<table width="600" border="0" cellspacing="0" cellpadding="0">
																<tbody>
																	<tr>
																		<td bgcolor="#ffffff" align="center" width="500">
																			
																		</td>
																	</tr>
																	<tr>
																		<td bgcolor="#ffffff" width="500" align="center" style="font-family:'Open sans',Arial,sans-serif;font-size:16px;line-height:24px;color:#4c4c4c;padding:0 50px">
																			<b>Order Questions?</b><br />
																			Call: &nbsp;
																			<span style="color:#4c4c4c"><? echo $phone ?></span>
																			<br><br>
																		</td>
																	</tr>
																</tbody>
															</table>
															<table width="600" border="0" cellspacing="0" cellpadding="0">
																<tbody>
																	<tr>
																		<td bgcolor="#ffffff" align="center" width="500">
																			<?
																			/*
																				* @hooked WC_Emails::order_details() Shows the order details table.
																				* @hooked WC_Structured_Data::generate_order_data() Generates structured data.
																				* @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
																				* @since 2.5.0
																				*/
																			do_action('woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email);
																			?>
																		</td>
																</tbody>
															</table>

														</div>
													</td>
												</tr>
											</table>
											<!-- End Content -->
										</td>
									</tr>
								</table>
								<!-- End Body -->
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top">
					<!-- Footer -->
					<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
						<tr>
							<td valign="top">
								<table border="0" cellpadding="10" cellspacing="0" width="100%">
									<tr>
										<td colspan="2" valign="middle" id="credit">
											<?php echo wp_kses_post(wpautop(wptexturize(apply_filters('woocommerce_email_footer_text', get_option('woocommerce_email_footer_text'))))); ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- End Footer -->
				</td>
			</tr>
		</table>
	</div>
</body>
</html>