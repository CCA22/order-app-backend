<?php

/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

if (!defined('ABSPATH')) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
//do_action( 'woocommerce_email_header', $email_heading, $email ); 
?>

<?php /* translators: %s: Customer first name */ ?>
<p><?php //printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); 
	?></p>
<p><?php //esc_html_e( 'We have finished processing your order.', 'woocommerce' ); 
	?></p>

<?
$order_id = $order->get_id();
$date = get_post_meta($order_id, 'pi_system_delivery_date', true);
$time = get_post_meta($order_id, 'pi_delivery_time', true);
$location = get_post_meta($order_id, 'pickup_location', true);
$phone = get_post_meta($order_id, 'pickup_location_phone', true);
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>" />
	<title><?php echo get_bloginfo('name', 'display'); ?></title>
</head>

<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
	<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'; ?>">
		<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
			<tr>
				<td align="center" valign="top">
					<div id="template_header_image">
						<?php
						if ($img = get_option('woocommerce_email_header_image')) {
							echo '<p style="margin-top:0;"><img src="' . esc_url($img) . '" alt="' . get_bloginfo('name', 'display') . '" /></p>';
						}
						?>
					</div>
					<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container">
						<tr>
							<td align="center" valign="top">
								<!-- Header -->
								<table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_header">
									<tr>
										<td id="header_wrapper">
											<h1><?php echo $email_heading; ?></h1>
										</td>
									</tr>
								</table>
								<!-- End Header -->
							</td>
						</tr>
						<tr>
							<td align="center" valign="top">
								<!-- Body -->
								<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
									<tr>
										<td valign="top" id="body_content">
											<!-- Content -->
											<table border="0" cellpadding="20" cellspacing="0" width="100%">
												<tr>
													<td valign="top">
														<div id="body_content_inner">
															<table width="600" border="0" cellspacing="0" cellpadding="0">
																<tbody>
																	<tr>
																		<td bgcolor="#ffffff" width="500" align="center" style="font-family:'Open sans',Arial,sans-serif;line-height:28px;color:#434a47;font-size:18px;padding:0 50px">
																			<b>Thank you!</b>
																			<br>
																			Here’s the pickup location:
																			<br>
																			<br>
																			<div style="font-family:'Open sans',Arial,sans-serif;font-weight:bold">
																				<b>The Happy Mixer

																				</b>
																			</div>

																		</td>

																	</tr>

																</tbody>
															</table>
															<table width="600" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding:0 50px">

																<tbody>
																	<tr>
																		<td height="24">

																		</td>
																	</tr>

																	<tr>

																		<td height="1" bgcolor="#c1c6c5" style="font-size:1px;line-height:1px">
																			<hr />
																		</td>

																	</tr>

																	<tr>

																		<td height="30">

																		</td>

																	</tr>

																</tbody>
															</table>
															<table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">

																<tbody>
																	<tr>

																		<td width="50" bgcolor="#ffffff">
																			<img style="display:block" src="https://ci4.googleusercontent.com/proxy/v7xfoh51VLz9wl77fVMhrLta-PFVzaxHPioqDo3oY1n0dJymxkKSlAZwm0V8mXz3no7Csg65OExCMPprRzUy64Tq4mdGbs1Q5yeS88Tn7yZaebrrnp33BwIZt9UnNB62q2Zrn9KoPFkxEMeHRurbllYD23GkgC-j_ylak1exPdIS1Q=s0-d-e1-ft#https://appboy-images.com/appboy/communication/assets/image_assets/images/5ad82598a12f74326a12eb73/original.gif" width="50" height="1" class="CToWUd">
																		</td>

																		<td>
																			<table width="500" border="0" cellspacing="0" cellpadding="0">
																				<tbody>
																					<tr>
																						<td bgcolor="#ffffff" width="500" align="center" style="font-family:'Open sans',Arial,sans-serif;font-size:19px;line-height:45px;color:#434a47">
																							<span style="line-height:36px">
																								Your order will be ready for pickup on:
																							</span>
																							<div style="font-family:'Open sans',Arial,sans-serif;font-weight:bold;font-size:24px;text-decoration:none">
																								<? esc_html($date) ?>
																								between
																								<? esc_html($time) ?>
																							</div>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																		<td width="50" bgcolor="#ffffff">
																			<img style="display:block" src="https://ci4.googleusercontent.com/proxy/v7xfoh51VLz9wl77fVMhrLta-PFVzaxHPioqDo3oY1n0dJymxkKSlAZwm0V8mXz3no7Csg65OExCMPprRzUy64Tq4mdGbs1Q5yeS88Tn7yZaebrrnp33BwIZt9UnNB62q2Zrn9KoPFkxEMeHRurbllYD23GkgC-j_ylak1exPdIS1Q=s0-d-e1-ft#https://appboy-images.com/appboy/communication/assets/image_assets/images/5ad82598a12f74326a12eb73/original.gif" width="50" height="1" class="CToWUd">
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</td>
												</tr>
											</table>
											<!-- End Content -->
										</td>
									</tr>
								</table>
								<!-- End Body -->
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top">
					<!-- Footer -->
					<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
						<tr>
							<td valign="top">
								<table border="0" cellpadding="10" cellspacing="0" width="100%">
									<tr>
										<td colspan="2" valign="middle" id="credit">
											<?php echo wp_kses_post(wpautop(wptexturize(apply_filters('woocommerce_email_footer_text', get_option('woocommerce_email_footer_text'))))); ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- End Footer -->
				</td>
			</tr>
		</table>
	</div>
</body>

</html>

<?php






















/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action('woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email);

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
//do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
//do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * Show user-defined additional content - this is set in each email's settings.
 */
// if ( $additional_content ) {
// 	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
// }

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
//do_action( 'woocommerce_email_footer', $email );
