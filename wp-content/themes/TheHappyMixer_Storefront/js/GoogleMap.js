/***
* Chris Axt
* Google Maps API v3
***/

var _api_key = "AIzaSyCS2qHP3HrEHiUXTCFhn6OMFFF6j9rvurQ";
var _location = "4275 County Line Road Chalfont, PA 18914";

//map object
var map;
//infoWindow
var infoWindow;
//geocoder object
var geocoder = new google.maps.Geocoder();
//stores current map markers - instantiate the markers array
var currentMapMarkers = new Array();
//service call
var service;
//map width
var mapWidth;
//map height
var mapHeight;


// Load Map
function LoadMap(startingLocation) {
	// Geocode the supplied location
	var homelocation = geocoder.geocode({ 'address': startingLocation }, function (results, status) {
		//if it was geocoded successfully
		if (status == google.maps.GeocoderStatus.OK) {
		    //set the location property from the result

			var location = results[0].geometry.location;

			// Call the create map function using the current location
			CreateMap(location, 15, 'map-canvas');
		}
	});
}

function CreateMarker(location) {
	var marker = new google.maps.Marker({
	     map: map,
         position: location,
         title: "The Happy Mixer Gluten Free Bakery"
	});

var contentString = '<div><strong style="font-size:12px;">The Happy Mixer Gluten Free Bakery</strong>'+
					'<br /><span style="font-size:12px;line-height:12px;">New Britain Village'+
					'<br />4275 County Line Road'+
					'<br />Chalfont, PA 18914</span>'+
					'<br /><a style="font-size:12px;" target="_blank" href="https://www.google.com/maps/preview#!q=4275+County+Line+Road+Chalfont%2C+PA+18914&data=!4m10!1m9!4m8!1m3!1d3651!2d-75.2275729!3d40.2653336!3m2!1i1680!2i921!4f13.1">Get Directions</a></div>';

	var infowindow = new google.maps.InfoWindow({
	    content: contentString
	});


	// Create info window
	/*var infowindow = new google.maps.InfoWindow({
		//content: contentString,
		//maxWidth: 20
	});*/

	// Add listner for marker
	google.maps.event.addListener(marker, "click", function() {
		infowindow.open(map, marker);
	});	
}


// Create Map
function CreateMap(location,zoom,elementID) {
	var mapOptions = {

	}

	//instantiate the map object for the given div
	map = new google.maps.Map(document.getElementById(elementID), {
		//set the map type
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		//set the center ( long , lat )
		center: location,
		//set the zoom level
		zoom: zoom,
		panControl: false,
		scaleControl: false
	});

	CreateMarker(location);


}

// 
function InitGoogleMap() {
	LoadMap("4275 County Line Road Chalfont, PA 18914");
}