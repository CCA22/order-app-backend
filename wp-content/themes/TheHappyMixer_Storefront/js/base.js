﻿jQuery(function ($)
{
    //match product action heights to line up add to cart buttons
    $('.product-actions h3').matchHeight();    

    var owlOverlay = $(".owl-carousel-item-imgoverlay");
    var owlControls = $(".owl-controls");
    //overlay exists
    if (owlOverlay.length > 0) {
        //remove
        owlOverlay.remove();
    }
    //control panel exists
    if (owlControls.length > 0) {
        //remove
        owlControls.remove();
    }
    if ($("#content").val() == "true") {
        $('.main-wrap .content').addClass("content-page");
    }
    if ($("#menu").val() == "true") {
        $('.main-wrap .content').addClass("menu-page");
    }

    var qtyIncrease = $("#increase-quantity");
    var qtyDecrease = $("#decrease-quantity");
    var qtyInput = $("input[id=quantity]");
    //increase product quantity
    qtyIncrease.on("click", function () {
        var oldValue = qtyInput.val();
        qtyInput.val(parseInt(oldValue) + 1);
    });
    //decrease product quantity
    qtyDecrease.on("click", function () {
        var oldValue = qtyInput.val();
        if (oldValue > 1) {
            qtyInput.val(parseInt(oldValue) - 1);
        }
    });

    if (window.innerWidth > 767) {

        $("li.menu-item-has-children").bind({
            mouseenter: function (e) {
                $(this).find(".sub-menu").slideDown(400);
                $(this).find("a").css("color", "000");
            },
            mouseleave: function (e) {
                $(this).find(".sub-menu").hide();
                $(this).closest("a").css("color", "#fff");
            }
        });
    }

    var bodyEl = document.body,
    page = document.querySelector('.page-wrap'),
    nav = document.querySelector('.menu-mobile');
    openbtn = document.getElementById('open-button'),
    closebtn = document.getElementById('close-button'),
    headerTop = document.getElementById('header-top')
    isOpen = false;  

    function init() {
        initEvents();
    }

    function initEvents() {
        openbtn.addEventListener('click', toggleMenu);
        if (closebtn) {
            closebtn.addEventListener('click', toggleMenu);
        }

        // close the menu element if the target it´s not the menu element or one of its descendants..
        page.addEventListener('click', function (ev) {
            var target = ev.target;
            if (isOpen && target !== openbtn) {
                toggleMenu();
            }
        });
    }

    function toggleMenu() {
        if (isOpen) {
            classie.remove(headerTop, 'mobile-menu-open');
            classie.remove(nav, 'menu-open');
            classie.remove(bodyEl, 'menu-push');
        }
        else {
            classie.add(headerTop, 'mobile-menu-open');
            classie.add(nav, 'menu-open');
            classie.add(bodyEl, 'menu-push');
        }
        isOpen = !isOpen;
    }

    init();

    $(window).scroll(function () {
        if ($(this).scrollTop() > 32) {
            classie.add(headerTop, 'fixed');
        } else {
            classie.remove(headerTop, 'fixed');
        }
    });
    $(function () {        
         $('.button.add_to_cart_button.product_type_simple, .single_add_to_cart_button.button.alt').on('click', function(e) { e.preventDefault(); });
         $('.button.add_to_cart_button.product_type_simple, .single_add_to_cart_button.button.alt').attr('href', '#');
         $('.button.add_to_cart_button.product_type_simple, .single_add_to_cart_button.button.alt').attr('title', 'Due to the national emergency COVID-19 virus, we have suspended shipping temporarily.');
         $('.button.add_to_cart_button.product_type_simple, .single_add_to_cart_button.button.alt').attr('disabled', 'disabled');
         $('.button.add_to_cart_button.product_type_simple').removeAttr('data-product_id');
         $('.button.add_to_cart_button.product_type_simple').removeAttr('data-product_sku');
         $('.button.add_to_cart_button.product_type_simple').removeAttr('data-quantity');

	var shop_cookie = docCookies.getItem("shop_modal");
        //setting up the default style
        vex.defaultOptions.className = 'vex-theme-os';
        //gets the current url path
        var currPath = document.location.pathname;

        //only open modal on shop page and if cookie has not already been set
        var dialogContent = "<div class='shipping-policy-container'><h2>Shipping and Return Policy</h2><div class='shipping-policy'>" +
                            "<ol>" +
                                "<li>We ship via USPS Priority Mail or USPS Express Mail. All orders will ship on Tuesdays. Orders placed by Sunday evening will be shipped on Tuesday. Orders placed Monday and Tuesday will ship the following Tuesday.</li>" +
				"<li>You can trust that your order will be freshly baked and carefully packaged the morning of your order&rsquo;s shipment.&nbsp;&nbsp; However, <em><strong>we do not offer&nbsp;returns,</strong> <strong>refunds or exchanges</strong></em>.&nbsp; If you have any questions, please feel free to email us at <a href=\"mailto:sales@thehappymixer.com\">sales@thehappymixer.com</a> <em><strong>prior</strong></em> to placing an order.</li>" +
				"<li>Please keep in mind, when your goodies arrive, if you do not plan on consuming them in a day or two, it is best to freeze them. We do not add preservatives to our goodies to extend shelf life!</li>" +
				"<li>We respect your privacy. Any and all information collected on this site will be kept strictly confidential and will not be sold, disclosed to third parties or reused without your permission. Any information you provide will be held with care.</li>" +
                            "</ol>" + 
                            "</div></div>";

        if ( (currPath.indexOf('/thehappymixer/shop/') == 0) || (currPath.indexOf('/shop/') == 0) )
        {
            if(shop_cookie == null) 
            {
                vex.open({
                    content: dialogContent,
                    afterClose: function () {                    
                        //get current date
                        var cookieExpirationDate = new Date();
                        //set cookie expiration date equal to 1 day
                        cookieExpirationDate.setDate(cookieExpirationDate.getDate() + 1);
                        //set the cookie
                        docCookies.setItem('shop_modal', "yes");
                    }
                });
            }
        }

        /** image zoom **/
        $(".woocommerce-main-image.zoom").click(function (e) {
            e.preventDefault();
            //vex.open({
            //    content: '<img width="350" height="350" src="http://thehappymixer/wp-content/uploads/2015/05/CC-Brownie-350x350.jpeg" class="attachment-shop_single wp-post-image" alt="CC Brownie" title="CC Brownie">'
            //});
        });
    });




});


